%% Load the meta file(s)
close all; clear all;clc;

headFolder = 'C:\Data\CABOW Field Testing\playbacks_subset_20210428';
sourceRecordingFiles = dir('C:\Data\CABOW Field Testing\playbacks_subset_20210428\SL_recordings\*wav*');
receivedRecordings = dir('C:\Data\CABOW Field Testing\playbacks_subset_20210428\CABOW_ID\pb325\recordings\*.wav');


meta =readtable(fullfile(headFolder, 'playbacks.csv'))
clipTable =readtable('C:\Data\CABOW Field Testing\playbacks_subset_20210428\CABOW_ID\pb325\TableExports\ClassifierUpcallNoise.csv');
edgeTable = readtable('C:\Data\CABOW Field Testing\playbacks_subset_20210428\CABOW_ID\pb325\TableExports\RightWhale.csv');
SB_latLon = [48.40735	-122.98018333333]


deadtimes = readtable(   fullfile(headFolder, 'CABOW_ID\pb325\325_20210428_DeadTimes.csv'));

CABOW_e2e = 158.9 ;


%% Create dataframe with detections, source levels and
%1) Get the source level information

SLtable  = [];
matlabCal =-213
for jj = 1:length(sourceRecordingFiles)
    [Sourceyy, fs1] = audioread(fullfile(sourceRecordingFiles(jj).folder,...
        sourceRecordingFiles(jj).name));
    
    % File date
    fDate = datenum(sourceRecordingFiles(jj).name(5:end-4), 'yyyymmdd_HHMMSS_fff');
    
    
     if any([17 21 23] == jj) 
        [peaks, locs] = findpeaks(Sourceyy(fs1:fs1*60*5),  'MinPeakDistance',...
            fs1*7,'MinPeakHeight',.0015);
      
     else
        % Signal peaks

        [peaks, locs] = findpeaks(Sourceyy(fs1:end),  'MinPeakDistance',...
            fs1*7,'MinPeakHeight',.0015);
     end
    
    
    peakStart = locs-(.4*fs1)+fs1;
    peakStop = locs+(.4*fs1)+fs1;
    SL =[];
    filteredYY = bandpass(Sourceyy, [50 225], fs1,'ImpulseResponse','iir','Steepness',0.5);
    for ii =1:length(peakStart)
        
        SL(ii) = rms(filteredYY(peakStart(ii):peakStop(ii)));
        
    end
    
    if length(peakStart)==27
        callType = [repmat({'Parks'}, [9,1]);...
            repmat({'Simulated'}, [10,1]);...
            repmat({'DCLDE'}, [8,1])];
    else
        
        if jj==1
            peaks = peaks(1:end-1);
            locs = locs(1:end-1);
            SL = SL(1:end-1);
            disp('bugger')
            callType = [repmat({'Parks'}, [9,1]);...
                repmat({'Simulated'}, [10,1]);...
                repmat({'DCLDE'}, [8,1])];
        end
        
        if jj==6
            
            callType = [repmat({'Parks'}, [9,1]);...
                repmat({'Simulated'}, [7,1])];
        end
        
        if jj==24
            
            callType = [repmat({'Parks'}, [9,1]);...
                repmat({'Simulated'}, [4,1])];
        end
    end
    
    matlabDate = fDate+((locs+fs1)/(24*60*60*fs1));
    
    
    % Get the drift associated with the playback
    [minVal, idx] = min(abs(matlabDate(1)-(datenum(meta.TimeUTC+meta.Date))));
    
    PeakTimes = datetime(matlabDate,'ConvertFrom','datenum');
    
    % Meta to dates str
    driftTime = meta.Date + meta.TimeUTC
    driftTime.Format = 'yyyy-MM-dd HH:mm:ss'
    % Estimate range between times
    LatInterp = interp1([driftTime(jj) PeakTimes(end)],...
        [meta.Lat_DecimalHours(jj) meta.LatEnd_DecimalHours(jj)],PeakTimes );
    LonInterp = interp1([driftTime(jj) PeakTimes(end)],...
        [meta.Lon_DecimalHours(jj) meta.LonEnd_DecimalHours(jj)], PeakTimes);
    
    [range]  = vdist2(...
        ones(length(LatInterp),1)*SB_latLon(1),...
        ones(length(LatInterp),1)*SB_latLon(2),...
        LatInterp, LonInterp);

    playbackId = repmat(meta.PlaybackNumber(idx), [length(matlabDate),1]);
    lat=LatInterp;
    lon = LonInterp;
    SLdB = (20*log10(SL'))-matlabCal ;
    depth = ones(size(lat))*8;
    callNum = [1:length(LonInterp)]';   
    
    PlaybackId = ones(size(lon))*jj;
    SLtable  = [SLtable; table(SLdB, matlabDate, callType,...
        range, lat, lon, PlaybackId, depth, callNum)];
    
    
end

[s,a12,a21] = vdist2( ...
    ones(height(SLtable),1)*SB_latLon(1), ...
    ones(height(SLtable),1)*SB_latLon(2),...
    SLtable.lat,...
    SLtable.lon);
SLtable.A12= a12;


SLtable.UnitLat= zeros(height(SLtable),1)+SB_latLon(1);
SLtable.UnitLON= zeros(height(SLtable),1)+SB_latLon(2);

SLtable.ExpArrival = SLtable.matlabDate+    (SLtable.range/1462)/60/60/24;
SLtable = sortrows(SLtable,'range','ascend'); % Sort by descending 


SLtable.RecRMSnoise = nan(height(SLtable),1);
SLtable.RecRMSsignal = nan(height(SLtable),1);
SLtable.BearingEst = nan(height(SLtable),1);
SLtable.ReceivedTimeNoiseRMSDB =zeros(height(SLtable),1);
SLtable = sortrows(SLtable,'matlabDate','ascend');
clearvars -except SLtable headFolder sourceRecordingFiles receivedRecordings meta clipTable edgeTable SB_latLon CABOW_e2e

%% Get the noise level at the sensor for each arrival time

receivedTableLoc = struct2table(receivedRecordings);
receivedTableLoc.MatlabDate = cell2mat(vertcat(cellfun(@(x) datenum(x(end-22:end-4),...
    'yyyymmdd_HHMMSS_fff'),receivedTableLoc.name, 'UniformOutput', false)));

receivedTableLoc.StartDate =...
    cellfun(@(x) datetime(x(end-22:end-4),...
    'InputFormat','yyyyMMdd_HHmmss_SSS'),receivedTableLoc.name);

receivedTableLoc.HumanReadable = datestr(receivedTableLoc.MatlabDate, 'yyyymmdd_HHMMSS')
receivedTableLoc = sortrows(receivedTableLoc,'MatlabDate','ascend');

fs = 2000;
badIDX=[]
for ii = 1:height(SLtable)
    
    try
        
        
        %fileIdx = find(receivedTableLoc.MatlabDate<SLtable.ExpArrival(ii)-(1/(60*60*24*fs)), 1, 'last');
        fileIdx = find(receivedTableLoc.StartDate<...
            (datetime(SLtable.ExpArrival(ii), 'ConvertFrom', 'datenum')-seconds(1)),1,'last');
        finfo = audioinfo(fullfile(receivedTableLoc.folder{fileIdx},receivedTableLoc.name{fileIdx}));

        
        % Time difference to identify the samples
        aa = (receivedTableLoc.StartDate(fileIdx) + seconds(finfo.Duration))-(datetime(SLtable.ExpArrival(ii), 'ConvertFrom', 'datenum')-seconds(1));
        startSec = seconds(finfo.Duration)-aa;
        startSamp = seconds(round(startSec*fs));
        stopSamp = startSamp+fs;
    
        if stopSamp>finfo.TotalSamples
            
            % load the audio
            samps = audioread(fullfile(receivedTableLoc.folder{fileIdx},...
                receivedTableLoc.name{fileIdx}),...
                [startSamp finfo.TotalSamples]);
            samps = [samps;...
                audioread(fullfile(receivedTableLoc.folder{fileIdx+1},...
                receivedTableLoc.name{fileIdx+1}),...
                [1 fs-length(samps)])];
        else
            
            % read in the audio segment
            samps = audioread(...
                fullfile(receivedTableLoc.folder{fileIdx},receivedTableLoc.name{fileIdx}),...
                [startSamp stopSamp]);
            
        end
        % Filster over noise band
        samps = samps(:,1);
    samps= bandpass(samps,[50 225],fs);
    
    
    movrms = 20*log10(sqrt(movmean(samps .^ 2, fs/4)))+CABOW_e2e;
    %     plot(movrms)
    
    SLtable.ReceivedTimeNoiseRMSDB(ii) = min(movrms);
    ii
    catch
        badIDX = [badIDX, ii];
    end

    
end

SLtable(badIDX,:)=[];

%receivedTableLoc

%% 2) Merge the clip table and the edge detection table to estimate bearings

% appears there is a one second offset between the utc in the edge detector
% and the utc in the clip generator (whyyyyyyyyyyy? and more importantly is
% this consistant? Can we count oan a one second delay always?)

edgeTable.UTC =edgeTable.UTC -seconds(1)
clipTable.BearingEst = nan(height(clipTable),1);

for ii=1:height(clipTable)

    [minTimediff(ii), idx] = (min(abs(clipTable.UTC(ii)-edgeTable.UTC)));
    
    clipTable.BearingEst(ii) = edgeTable.Angle_0(idx);
    

end

TPclips = clipTable(clipTable.Upcall==1,:);

% Inspect the data
scatter(clipTable.Id, clipTable.BearingEst, [], clipTable.Upcall, 'filled')

scatter(TPclips.RMSsignal-TPclips.RMSnoise, TPclips.BearingEst, [], TPclips.Upcall, 'filled')

xlabel('Clip Number'); ylabel('Bearing')
%% Now merge the source level table with the clip measurements

% temporary variable for cleaning it out 
clipTableTemp =clipTable;

SLtable.RecRMSnoise = nan(height(SLtable),1);
SLtable.RecRMSsignal = nan(height(SLtable),1);
% SLtable.BearingEst = nan(height(SLtable),1);
SLtable.detected = zeros(height(SLtable),1);
SLtable.Score = nan(height(SLtable),1);
SLtable.id = nan(height(SLtable),1);


for ii=1:height(SLtable)
    
     [minTimediff, idx] = (min(abs(...
         datetime(SLtable.ExpArrival(ii),'ConvertFrom','datenum')-...
         clipTableTemp.UTC)));
     
     if minTimediff <= seconds(5)
         
          SLtable.RecRMSnoise(ii)=clipTableTemp.RMSnoise(idx);
        SLtable.RecRMSsignal(ii)=clipTableTemp.RMSsignal(idx);
         SLtable.BearingEst(ii) = clipTableTemp.BearingEst(idx);
        SLtable.detected(ii)=1;
        SLtable.id(ii)=clipTableTemp.Id(idx);
        SLtable.Score(ii)=clipTableTemp.Score(idx);
         clipTableTemp(idx,:)= [];
     end
     
    
end

% Bearing Estimate
bearingDeg = ( SLtable.BearingEst*180)/pi;
SLtable.BearingEst= rem((bearingDeg+360), 360);
SLtable.BearingEst =bearingDeg;

SLtable.recSNR = SLtable.RecRMSsignal-SLtable.RecRMSnoise;
figure
scatter(SLtable.range, SLtable.SLdB, [], categorical(SLtable.callType), 'filled')
legend('DCLDE', 'Parks', 'Simulated')

calltypes =unique(SLtable.callType)
figure(20)
for ii =1:length(calltypes)
    datasub = SLtable(find(strcmp(SLtable.callType, calltypes(ii))),:);
    
    subplot(3,1,ii)
    scatter(datasub.range, datasub.recSNR, [], datasub.Score, 'filled')
    ylim([0 20])
    xlim([0 2500])
    title(calltypes(ii))
    caxis([0 1])
      xlabel('Range')
    ylabel('SNR')
    

end

figure(21)
for ii =1:length(calltypes)
    datasub = SLtable(find(strcmp(SLtable.callType, calltypes(ii))),:);
    
    subplot(3,1,ii)
    scatter(datasub.recSNR, datasub.Score, 'filled')
    title(calltypes(ii))
    xlabel('SNR')
    ylabel('Detector Score')
    
    xlim([0 30])

end

subplot(3,1,1)
title('DCLDE (detector training data)')

%% Propagation Loss
PLsub = SLtable(~isnan(SLtable.id),:);
PLsub = PLsub(find(strcmp(PLsub.callType, {'DCLDE'})),:);
PL = ((PLsub.SLdB-PLsub.RecRMSsignal)./log10(PLsub.range));

scatter(PLsub.range, PL,'filled')
xlabel('Range')
ylabel('PL coef')

scatter(PLsub.range, PL, [], PLsub.callNum, 'filled')
xlabel('Range')
ylabel('PL coef')


%% Signal Excess Detection Function g(r|SLNR)

SLtable.SLNL = SLtable.SLdB-SLtable.ReceivedTimeNoiseRMSDB


minDetRange = min(SLtable.range)
maxDetRange = max(SLtable.range)
sourceSub = SLtable(SLtable.range>minDetRange,:);

SLNRbins = min(SLtable.SLNL):10: max(SLtable.SLNL)+20;
RangeBins =[0:500:15000]


nPlayedr=[];
nDetr = [];
figure(14)

for ii = 1:length(SLNRbins)-1
    
    dataSubSNR = SLtable(find(SLtable.SLNL>SLNRbins(ii) &...
        SLtable.SLNL<=SLNRbins(ii+1)),:);
    
    for jj=1:length(RangeBins)-1
        dataSub = dataSubSNR(find(dataSubSNR.range>RangeBins(jj) &...
            dataSubSNR.range<=RangeBins(jj+1)),:);
        
        
        nPlayedr(ii,jj) = height(dataSub)
        nDetr(ii,jj) = sum(dataSub.detected)
        
        
    end
    
end


x= repmat(1:size(nDetr,2),[(size(nDetr,1)),1]);
y= repmat(1:size(nDetr,1),[(size(nDetr,2)),1])'
pcolor(RangeBins(1:end-1),SLNRbins(1:end-1), nDetr./nPlayedr)
ylabel('SLNL')
xlabel('Range')






%%


% Aggregate by range
dataSub = SLtable(find(strcmp(SLtable.callType, {'DCLDE'})),:);
rangeBins = [0:200:max(PLsub.range)+200];
nPlayed=[];
nDet = [];
for ii=1:length(rangeBins)-1
    
    idx = find(dataSub.range>rangeBins(ii) & dataSub.range<=rangeBins(ii+1));
    nPlayed(ii) = length(idx);
    nDet(ii) = sum(dataSub.detected(idx));
    
    
end


propDet = nDet./nPlayed
[vals,~, g] = unique(SLtable.callType)

figure(2)
scatter(SLtable.range, SLtable.id,[], SLtable.detected, 'filled')

xlabel('Range')
%% Figure out the bearing error


% Bearing offset, unknown angle of the buoy

BearingOffset = median(abs(SLtable.BearingEst(SLtable.range>500 & SLtable.range<5000)-...
    SLtable.A12(SLtable.range>500 & SLtable.range<5000)), 'omitnan')
% 
% NewBearingError = abs(SLtable.BearingEst-SLtable.Azmuth)-BearingOffset
NewBearingError1 = abs(SLtable.BearingEst-SLtable.A12)-BearingOffset
NewBearingError1(NewBearingError1>180)= mod(NewBearingError1(NewBearingError1>180),180)
NewBearingError1(NewBearingError1<-180)= mod(NewBearingError1(NewBearingError1<-180),180)


SLtable.BearingError = NewBearingError1;

%%

writetable(SLtable, 'C:\BitBucketRepositories\CABOWProcessing\ProcessedCSVfiles\20210428_325.csv')
