function [Precision Recall] = cabowPrecRecall(SLtable, clipTable)





% Detection scores greater than the thresh and represent a true positive
thresh= [0:.05:1];
SLtable.Score(isnan(SLtable.Score))=-inf;
SLtable.detected(isnan(SLtable.detected))=0;

TP = sum(bsxfun(@gt,SLtable.Score,thresh).*...
    repmat(SLtable.detected,[1, length(thresh)]), 'omitnan')

FP = sum(bsxfun(@gt,clipTable.Score,thresh).*...
    repmat(clipTable.FP,[1, length(thresh)]), 'omitnan');

% Calls in the truth table that were not assigned a score
FN  =sum(bsxfun(@lt,SLtable.Score, thresh).*...
    repmat(~SLtable.detected,[1, length(thresh)]), 'omitnan'); 

Prec = TP./(TP+FP);
Recall =TP./(TP+FN)

plot(Prec,Recall); ylabel('Recall'); 

end