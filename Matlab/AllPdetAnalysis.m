%% Code for processing all playback files


close all; clear all; clc

% Load all the processed files
PdetFiles = [ {'C:\Data\CABOW Field Testing\20210317 HRS\20210317_HAR.csv'},...
    {'C:\Data\CABOW Field Testing\20210311 HRS\20210311_HAR.csv'},...
    { 'C:\Data\CABOW Field Testing\20210329 HRS\20210329_HAR.csv'}];


dataTable =[];
prevLength = 0;
for ii =1:length(PdetFiles)

    datatableIn =  readtable(PdetFiles{ii});
    datatableIn.PlaybackId = datatableIn.PlaybackId+prevLength;
    
    dataTable = [dataTable; datatableIn]
prevLength = prevLength+max(dataTable.PlaybackId);
end

dataTable.ClassifierScore(isnan(dataTable.ClassifierScore))=0;
dataTable.dateRounded = floor(dataTable.matlabDate)


% Bearing Offset
dataTable.Angle0deg = dataTable.Angle_0*(180/pi)
dataTable.Angle0deg(dataTable.Angle0deg<0) = dataTable.Angle0deg(dataTable.Angle0deg<0)+360;

bearingOffset = median(...
dataTable.Angle0deg(dataTable.ClassifierScore>.9) -...
dataTable.Azmuth(dataTable.ClassifierScore>.9))


dataTable.ActualBearingEst = dataTable.Angle0deg-bearingOffset;
dataTable.BearingError = dataTable.ActualBearingEst-dataTable.Azmuth;
hist(dataTable.BearingError)

scatter(dataTable.signal(dataTable.detected==1)./dataTable.noise(dataTable.detected==1), ...
    dataTable.BearingError(dataTable.detected==1),...
    [],dataTable.ClassifierScore(dataTable.detected==1))
%% Estimate Transmission loss 


dataTable.RL = 20*log10(dataTable.signal);
dataTable.NL = 20*log10(dataTable.noise);
dataTable.SE = dataTable.Var1- 20*log10(dataTable.noise);
dataTable.SNR = 20*log10(dataTable.signal) - dataTable.NL;

% How much did the signal drop
SigDecrease = dataTable.Var1-dataTable.RL

dataTable.TLcoef = SigDecrease./(-log10(dataTable.range));


%% Plot the detection function for each bearing
 close all
 
 dataTable = dataTable(dataTable.depth<5,:)
 dataTable=dataTable( find(strcmp(dataTable.callType, 'Parks')),:);

 % Aggregate by range
 rangeBins = [0:200:roundn(max(dataTable.range),2)]
 nPlayed=[];
 nDet = [];
 
 
 azmuthList = unique(dataTable.AzmuthBinned);
 azmuthList = azmuthList(~isnan(azmuthList));
 
 
 legendText = [];
 %Break into azmuth rings
 for jj = 1:length(azmuthList)
     datasub = dataTable(dataTable.AzmuthBinned==azmuthList(jj),:);
     
     for ii=1:length(rangeBins)-1
         
         nPlayed(ii) = sum(datasub.range>rangeBins(ii) & datasub.range<=rangeBins(ii+1))
         nDet(ii) = sum(datasub.range>rangeBins(ii) & datasub.range<=rangeBins(ii+1)...
             &datasub.Upcall==1)
  
     end
     
    
     

     propDet = nDet./nPlayed;
     x = rangeBins(2:end);
     
     x= x(~isnan(propDet));
 y= propDet(~isnan(propDet));
 

 h(jj) = scatter(x, y,'filled')
 legendText = [legendText, {[ 'Azmuth ' num2str(azmuthList(jj))]}]

 hold on
 end
 
 
 legendHandle = legend(legendText)
 xlabel('Range (m)')
 ylabel('Proportion Detected')
%% Aggregate by signal excess


nPlayed=[];
 nDet = [];
 
 
 SEbins = -30:30:75;
 
 
 legendText = [];
 %Break into azmuth rings
 figure
 for jj = 1:length(SEbins)-1
     datasub = dataTable(dataTable.SE>= SEbins(jj) & dataTable.SE<= SEbins(jj+1),:);
 
 for ii=1:length(rangeBins)-1
 
     nPlayed(ii) = sum(datasub.range>rangeBins(ii) & datasub.range<=rangeBins(ii+1))
     nDet(ii) = sum(datasub.range>rangeBins(ii) & datasub.range<=rangeBins(ii+1)...
     &datasub.Upcall==1)

 
 end
 
 
 propDet = nDet./nPlayed;
 x = rangeBins(2:end);
 
 x= x(~isnan(propDet));
 y= propDet(~isnan(propDet));
 

 h(jj) = plot(x, y, '.-','markersize',20)
 legendText = [legendText, {[ 'SEbin ' num2str(azmuthList(jj))]}]

 hold on
 end
 
 
 legendHandle = legend(legendText)
 xlabel('Range (m)')
 ylabel('Proportion Detected')






%% Plot bearings


r = dataTable.range;
x = sin(dataTable.Angle_0).*r;
y = cos(dataTable.Angle_0).*r;
center = zeros(size(x));

colorscale = jet(max(round(dataTable.ClassifierScore*100))+1)
scores=unique(dataTable.ClassifierScore);
scores=scores(scores>0)
[~,colorOrder] = sort(dataTable.ClassifierScore, 'descend');



figure
ax = axes;
ax.NextPlot = 'add';
ax.ColorOrder = colorscale(round(dataTable.ClassifierScore*100)+1,:);
plot([center x]', [center y]')



%% Plot Pdet vs Range
 % Aggregate by range

rangeBins = [0:300:roundn(max(dataTable.range),2)]
 nPlayed=[];
 nDet = [];
 for ii=1:length(rangeBins)-1
 
     nPlayed(ii) = sum(dataTable.range>rangeBins(ii) & dataTable.range<=rangeBins(ii+1))
     nDet(ii) = sum(dataTable.range>rangeBins(ii) & dataTable.range<=rangeBins(ii+1)...
     &dataTable.Upcall==1)

 
 end
 
 
 propDet = nDet./nPlayed
 
 figure(2)
 plot(rangeBins(2:end), propDet)
%% Plot Pdet vs Range and Bearing

rangeBins = [0:200:roundn(max(dataTable.range),2)]
 nPlayed=[];
 nDet = [];
 
 dataTable.AzmuthRounded = round(dataTable.Azmuth/50)*50;
 azmuthList = unique(dataTable.AzmuthRounded);
 azmuthList = azmuthList(~isnan(azmuthList));
 
 
 legendText = [];
 %Break into azmuth rings
 for jj = 1:length(azmuthList)
     datasub = dataTable(dataTable.AzmuthRounded==azmuthList(jj),:);
 
 for ii=1:length(rangeBins)-1
 
     nPlayed(ii) = sum(datasub.range>rangeBins(ii) & datasub.range<=rangeBins(ii+1))
     nDet(ii) = sum(datasub.range>rangeBins(ii) & datasub.range<=rangeBins(ii+1)...
     &datasub.Upcall==1)

 
 end
 
 
 propDet = nDet./nPlayed;
 x = rangeBins(2:end);
 
 x= x(~isnan(propDet));
 y= propDet(~isnan(propDet));
 

 h(jj) = plot(x, y, '.-','markersize',20)
 legendText = [legendText, {[ 'Azmuth ' num2str(azmuthList(jj))]}]

 hold on
 end
 
 
 legendHandle = legend(legendText)
 xlabel('Range (m)')
 ylabel('Proportion Detected')
