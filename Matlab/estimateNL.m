function NL = estimateNL(SLtable, clipTable)

    NL = SLtable.RecRMSnoise;
    % get the nan NL's 
    for ii=1:length(NL)
        if isnan(NL(ii))
           
            clipSub = clipTable(clipTable.CabowID==SLtable.CABOWid(ii),:);
            [~, idx]=min(abs(clipSub.UTC-SLtable.ExpArrival(ii)));
            NL(ii)= clipSub.RMSnoise(idx); 
        end
    end



end