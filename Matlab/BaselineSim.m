close all; clear all; clc
% received = readtable('C:\Data\CABOW DetFun Sims\temp\REC_321_ClassifierAndNoise.csv');
% source = readtable('C:\Data\CABOW DetFun Sims\temp\SRC_SIMULATED.csv');
% 
% source = readtable('C:\Data\CABOW DetFun Sims\TRIAL_20210521_03\Source\SRC_20210521_03_Reduced.csv');
% received = readtable('C:\Data\CABOW DetFun Sims\TRIAL_20210521_03\Received\pb321\REC_20210521_03_321_Combined.csv');

source = readtable('C:\Data\CABOW DetFun Sims\TRIAL_20210614_00\SRC\EdgeDetection.csv');
% Now there is a loclized noise source? Clean it out
source.Sound = categorical(source.Sound);
idx =source.Sound== 'Right Whale like ca';
source = source(idx,:);
clear idx

received = readtable(...
    'C:\Data\CABOW DetFun Sims\TRIAL_20210614_00\REC\NoiseClassifierCombined.csv');
edgeDetection = readtable('C:\Data\CABOW DetFun Sims\TRIAL_20210614_00\REC\RightWhale.csv');



% soundspeed (assumed)
c = 1500;

% 299: .22
lat=-1000*.22;lon=0; depth=-30

% 321
lat=.0;lon=00; depth=-30
% lat= -.002 ;lon=-.002; depth=-30
% lat= 0.0048 ;lon=0; depth=-30



% Range from source to receiver
[rng, az] = vdist2(repmat(lat, [height(source),1]),...
    repmat(lon, [height(source),1]),source.Latitude,source.Longitude);


source.Range= sqrt(rng.^2+(source.z-depth).^2);

source.trueBearing = az;
source.Range=rng;
expectedArrival = source.UTC+seconds(source.Range./c); %Check this with SAM/
source.ArrivalNL= zeros(height(source),1);
source.ArrivalRL= zeros(height(source),1);
source.Detected = zeros(height(source),1);
source.BearingEst= nan(height(source),1);
source.SNR= nan(height(source),1);
source.ReceivedTime = NaT(height(source),1);
source.expectedArrival=expectedArrival;


% % Get the bearing and ofset,
% trueBearing = asin(source.y./source.Range);
%measuredBearing = received.Angle_0

received.CallFound = zeros(height(received),1);

%% Link the source and receiver dataframs


for ii =1:1:height(received)
    
    % closest approach
    [mindiff, idx] = min(abs(expectedArrival-received.UTC(ii)));
    
    
    if seconds(mindiff)<4

        source.ArrivalNL(idx) = received.RMSnoise(ii);
        source.ArrivalRL(idx) = received.RMSsignal(ii);
        source.Detected(idx) = 1;
        %source.BearingEst(idx) = received.Angle_0(ii);
        source.SNR(idx) = received.SNR(ii);
        source.ReceivedTime(idx) = received.UTC(ii);
        received.CallFound(ii)=1;
        
        % Link to the bearing table
        [~, idx1 ] = min(abs(edgeDetection.UTC-received.UTC(ii)))
        source.BearingEst(idx) = edgeDetection.Angle_0(idx1);
        
        ii
    else
        disp('nope')
    end
    
    
    
end
figure;
scatter(source.Range, source.ArrivalRL, [], source.Detected, 'filled')
% Number of detecentions in each range bin
% Aggregate by range
sourceOrig= source;
rangeBins = [0:500:max(source.Range)+1000];
figure( 110)
scatter(sourceOrig.x,...
    sourceOrig.y, [],sourceOrig.Detected, 'filled')
hold on; scatter(0,0, 'r^', 'filled')
xlabel('Range (m)')
ylabel('Range (m)')


% Update values basedon the highest received levels
[~, idx] = max(source.ArrivalRL)


%%
figure; scatter(source.Longitude(source.Detected==1),...
source.Latitude(source.Detected==1), [],source.Range(source.Detected==1).^3, 'filled')
h = colorbar;
hold on; scatter(lon,lat, 'r^', 'filled')
ylabel(h, 'Received Level (dB)')
xlabel('Longitude')
ylabel('Latitute')



figure; 
scatter(source.y,source.x, 80, 'k')
hold on; 
scatter(source.y(source.Detected==1),source.x(source.Detected==1),...
    80,'k', 'filled')
ylim([min(source.y)-200 max(source.y)+200])
xlim([min(source.x)-200 max(source.x)+200])
scatter(0,0,200, [.5 .5 0.5],'^', 'filled')
xlabel('Range (m)')
ylabel('Range (m)')
legend({'Not Detected', 'Detected', 'CABOW Location'})


%% Inspect prelim det fx
nPlayed=[];
nDet = [];

for ii=1:length(rangeBins)-1
    
    idx = find(source.Range>rangeBins(ii) & source.Range<=rangeBins(ii+1));
    nPlayed(ii) = length(idx);
    nDet(ii) = sum(source.Detected(idx));
    
    
end
% 
% subplot(3,1,2)
% scatter(source.Longitude,source.Latitude, [],source.Detected)

%%
hold off
figure (110)
%subplot(2,1,1)
scatter(rangeBins(1:end-1), nDet./nPlayed, 'k', 'filled')
xlabel('Range (m)'); ylabel('Proportion Detected')

xx = 1:4500
hold on;
plot(xx, hazard([2400 6.9],xx))
legend({'Detections', 'Hazard Model'})

%% g(SNR)

% Since the source and receiver both played the sound we need to break it
% apart

% Figure out transmission loss
TL_coef = 20;
TL = (TL_coef*log10(source.Range(source.Detected==1)))
RL =  source.ArrivalRL(source.Detected==1);

% The noise was with the source
[aa]=find(source.Range<2000 & source.Detected==1);
SourceNL =source.ArrivalNL(aa);
clear aa
% 
% % Update noise was no longer the source

SL = RL+TL-source.ArrivalNL(source.Detected==1);
NL = source.ArrivalNL(source.Detected==1);

% Guess the source level and noise level for the missed calls
sampleIndex = randi(sum(source.Detected),[sum(~source.Detected),1]);
missedR = source.Range(source.Detected==0)
missedTL = TL_coef*log10(missedR)



missedNL = std(NL)/2*randn(1,length(missedR))'+ median(NL, 'omitnan')';
missedSL = std(SL)*randn(1,length(missedR))'+ median(SL, 'omitnan')';
missedRL =missedSL-missedTL+missedNL;


source.ArrivalNL(source.Detected==0) = missedNL;
source.ArrivalRL(source.Detected==0) = missedRL;


source.SNR1 = source.ArrivalRL-source.ArrivalNL;
figure
scatter(source.Range, source.SNR, [], source.Detected, 'filled')
xlabel('Range'); ylabel('SNR')


%source.SNR = source.ArrivalRL-source.ArrivalNL;
%source.SNR(source.SNR>100) =100;
% 
% scatter(source.Range, source.SNR, [], source.Detected, 'filled')
% scatter(source.Longitude, source.Latitude+rand([height(source),1]), [],...
%     source.Detected, 'filled')
% xlabel('longitude')

% 
% % Signal Excess
% SLNL = SL-source.ArrivalNL;
% scatter(source.Range, SLNL, [], source.Detected)
% source.SLNL = source.ArrivalRL-source.ArrivalNL;
% %source.SNR = SL-source.ArrivalNL

%% Detection Function g(SNR)

SNR_bins = min(source.SNR1):1: max(source.SNR1);
nPlayedr=[]
nDetr=[]
for ii = 1:length(SNR_bins)-1
    
    detectedSub = source(source.SNR1>= SNR_bins(ii)...
        & source.SNR1<SNR_bins(ii+1),:);
    nPlayedr(ii) = height(detectedSub)
    nDetr(ii) = sum(detectedSub.Detected)
    clear detectedSub
    
end
figure (1)
scatter(SNR_bins(1:end-1), nDetr./nPlayedr, 'filled')
xlabel('SNR'); ylabel('Proportion Detected')



% 
% 

% Number of detections each SNR

figure 
scatter(SNR_bins(1:end-1), nDetr./nPlayedr, 'k', 'filled')
xlabel('SNR (dB)'); ylabel('Proportion Detected')
b=-2; m=1;hold on
pdetSNR = @(x) 1./(1+exp(-(b+m*x)))
plot([-6:.1:14], pdetSNR([-6:.1:14]), 'r' )
legend({'Proportion Calls', 'Fitted Logistic Model'})
ylim([-.01 1.01])

 %% Bearing and bearing error err(SNR)
 
 

% True bearing
[~, BearingTheta] =vdist2(repmat(lat, [height(source),1]),...
    repmat(lon, [height(source),1]),source.Latitude,source.Longitude);

% Observed bearing
obsTheta =  180+(180*source.BearingEst/pi);
% shift it by 180 deg
source.shiftedTheta = obsTheta;
 source.shiftedTheta(obsTheta<180)=obsTheta(obsTheta<180)+180;
 source.shiftedTheta(obsTheta>=180)=obsTheta(obsTheta>=180)-180;


[min(source.shiftedTheta) max(source.shiftedTheta)]
[min(BearingTheta) max(BearingTheta)]
% 
% shiftedTruth =BearingTheta-180
% shiftedTheta(obsTheta>=-180)=obsTheta(obsTheta>=180)-180;


figure;
subplot(2,1,1)
scatter(source.Longitude, source.Latitude, [], source.shiftedTheta, 'filled'); colorbar
title('Estimated Bearing')

subplot(2,1,2)
scatter(source.Longitude(source.Detected==1), source.Latitude(source.Detected==1),...
    [],source.trueBearing(source.Detected==1), 'filled'); colorbar
title('True Bearing')

%% Bearing error
figure
subplot(2,1,1)
bearingError =abs(source.trueBearing-source.shiftedTheta);
scatter(source.SNR1(source.Detected==1),bearingError(source.Detected==1), [], 'filled')
xlabel('SNR')
ylabel('Bearing Error (deg)')

subplot(2,1,2)
scatter(source.Range,bearingError, [], 'filled')
xlabel('Range (m)')
ylabel('Bearing Error (deg)')

