
close all; clear all; clc

%% Load the meta file(s)

headFolder = 'C:\Data\CABOW Field Testing\playback_subset_20210202';
BinaryFiles = dir('C:\Data\CABOW Field Testing\playback_subset_20210202\CABOW_ID\pb318\binary\*RW*');
ClipBinaryFiles = dir('C:\Data\CABOW Field Testing\playback_subset_20210202\CABOW_ID\pb318\binary\*clip*');
playBackFiles = dir('C:\Data\CABOW Field Testing\playback_subset_20210202\SL_files\playback_files\*wav*');
sourceRecordingFiles = dir('C:\Data\CABOW Field Testing\playback_subset_20210202\SL_files\Reson_recordings\*wav*');

meta =readtable(fullfile(headFolder, '20210202_PLAYBACKNOTES.xlsx'))

%% Load the file played back and create structure for source level recordings

% Moving average filter
signalTime = [];
peakVals = [];
SimSignal =[];
for jj = 1:length(sourceRecordingFiles)
    [Sourceyy, fs1] = audioread(fullfile(sourceRecordingFiles(jj).folder,...
        sourceRecordingFiles(jj).name));

        % File date
        fDate = datenum(sourceRecordingFiles(jj).name(5:end-4), 'yyyymmdd_HHMMSS_fff');
        
        
        r = fs1/2000;
        yy = decimate(Sourceyy, r);
        bandPassedData = bandpass(yy,[78 500],2000);
    
        % Signal peaks 
        [peaks, locs] = findpeaks(bandPassedData(2000: end).^2,'MinPeakDistance', 2000*3.5)
        
        % Determine if real or simualted calls
        if(sum(peaks> 1e-6)>5)
            
            % more peaks therefore
            peakLocs = locs(peaks>1e-6)+2500;
            peaks = peaks(peaks>1e-6);
            realCallBool = ones(size(peaks));
        else
            
            % Signal peaks
            [peaks, locs] = findpeaks(bandPassedData(4000: end).^3,'MinPeakDistance', 2000*2)
            peakLocs = locs(peaks>2e-10)+4000;
            peaks = peaks(peaks>2e-10);
            realCallBool = zeros(size(peaks));
        end
        
        
        peakTimes = (peakLocs/2500)/60/60/24 +fDate;
        
        % Peak output
        SimSignal= [SimSignal;  realCallBool];
        
        % Time at which the signal was played
        signalTime = [signalTime; peakTimes];
        peakVals = [peakVals; peaks];
        
        
        
end

%% Link playback times with distances

% Cabow GPS
CABOW_lat_lon =[48.39090	-123.11710];

rangeM=zeros(size(peakVals));
azmuth = zeros(size(peakVals));
meta.dateNum = datenum(meta.TimeUTC)+datenum('20210202', 'yyyymmdd')



for ii=1:length(peakVals)
    
    % Index of the closest time
    [minVal, idx ]= min(abs(meta.dateNum-signalTime(ii)));
    
    [rangeM(ii), azmuth(ii)] = vdist2(meta.Lat(idx),meta.Long(idx), CABOW_lat_lon(1),CABOW_lat_lon(2));
    
 

end



% Create Data frame
playbackData = table(signalTime, SimSignal, rangeM,azmuth)
playbackData.Detected = zeros(height(playbackData), 1);
playbackData.BearingPredicted= zeros(height(playbackData), 1);
playbackData.Edgescore =zeros(height(playbackData), 1);
playbackData.UDPscore = zeros(height(playbackData), 1);
playbackData.EstArrival = (playbackData.rangeM/1500)/60/60/24+playbackData.signalTime;
playbackData.EstTravels =   (playbackData.rangeM/1432); 
playbackData.UID =  zeros(height(playbackData), 1);
playbackData.estArrivalUTC = datestr(playbackData.EstArrival);
playbackData.signalTimeUTC = datestr(playbackData.signalTime);


%% Run Cross correlation to determine the exact time at which the detections were produced

%% Load the Pamguard files and look for matching times

binaryTable = [];

% Create a table for the binary data
for ii= 1:length(BinaryFiles)

% load the binary file
[dataSet, fileInfo] = loadPamguardBinaryFile(...
    fullfile(BinaryFiles(ii).folder,BinaryFiles(ii).name))
if ~isempty(dataSet)

    binaryTable = [binaryTable; struct2table(dataSet,'AsArray', true)];
    
end

end

binaryTable.Capture = zeros(height(binaryTable),1)


% Compare with signal table
for ii = 1:height(playbackData)


    diffTime = abs(seconds( datetime(binaryTable.date,'ConvertFrom','datenum') -...
        datetime(playbackData.EstArrival(ii), 'ConvertFrom', 'datenum')));
    
    [minDiff, minIdx] = min(diffTime);
    
    if minDiff<2
    
        playbackData.UID(ii) = binaryTable.UID(minIdx);
        playbackData.Edgescore(ii) = binaryTable.type(minIdx);
        playbackData.Detected(ii)=1;
        binaryTable(minIdx,:)= [];
        
    end
    
    

end



UIDs  = unique(playbackData.UID)

%%
% Look at the clips for the UID's

% Create a table for the binary data
for ii= 1:length(ClipBinaryFiles)

% load the binary file
[dataSet, fileInfo] = loadPamguardBinaryFile(...
    fullfile(ClipBinaryFiles(ii).folder,ClipBinaryFiles(ii).name))

if (~isempty(dataSet) && ~isempty(dataSet(2).millis))

    
    
    for jj = 1:length(dataSet)
    spectrogram(dataSet(jj).wave,kaiser(256,18),128,256,2000, 'yaxis')
    disp('blarg')
    end
    
end

end





%%



for ii= 1:length(BinaryFiles)

% load the binary file
[dataSet, fileInfo] = loadPamguardBinaryFile(...
    fullfile(BinaryFiles(ii).folder,BinaryFiles(ii).name))


% Comparere the datetimes to the undetected
if ~isempty(dataSet)
    for jj=1:height(playbackData)
        
        timeDiff = abs(playbackData.EstArrival(jj)- vertcat(dataSet.date))*60*60*24;
        
        [tdiff, idx] = min(timeDiff);
        
        if tdiff<20
            playbackData.UID(ii) = dataSet(idx).UID;
            playbackData.Edgescore(ii) = dataSet(idx).type;
            playbackData.Detected(ii)=1;
        end
        
        
    end
end



end

