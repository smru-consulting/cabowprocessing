function  F = hazard(adjustments, x)
key=1-exp(-(x/adjustments(1)).^-adjustments(2));

F=key;
end
