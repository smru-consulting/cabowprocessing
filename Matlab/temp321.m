close all; clear all; clc
% received = readtable('C:\Data\CABOW DetFun Sims\temp\REC_321_ClassifierAndNoise.csv');
% source = readtable('C:\Data\CABOW DetFun Sims\temp\SRC_SIMULATED.csv');
% 
% source = readtable('C:\Data\CABOW DetFun Sims\TRIAL_20210521_03\Source\SRC_20210521_03_Reduced.csv');
% received = readtable('C:\Data\CABOW DetFun Sims\TRIAL_20210521_03\Received\pb321\REC_20210521_03_321_Combined.csv');

source = readtable('C:\Data\CABOW DetFun Sims\TRIAL_20210603_01\Source\EdgeDetection.csv');
% Now there is a loclized noise source? Clean it out
source.Sound = categorical(source.Sound);
idx =source.Sound== 'Right Whale like ca';
source = source(idx,:);
clear idx

received = readtable(...
    'C:\Data\CABOW DetFun Sims\TRIAL_20210603_01\Recieved\ClassifierNoiseCombined_321.csv');
edgeDetection = readtable('C:\Data\CABOW DetFun Sims\TRIAL_20210603_01\Recieved\RightWhale_321.csv');

badTimes= readtable('C:\Data\CABOW DetFun Sims\TRIAL_20210603_01\Recieved\321IgnoreTimes.csv');
badTimes.startUMT = datetime(badTimes.Start_Gap,'ConvertFrom','datenum');
badTimes.endUMT = datetime(badTimes.End_Gap,'ConvertFrom','datenum');



% soundspeed (assumed)
c = 1500;

% 299: .22
lat=-1000*.22;lon=0; depth=-30

% 321
lat=.0043;lon=-.002; depth=-30
% lat= -.002 ;lon=-.002; depth=-30
% lat= 0.0048 ;lon=0; depth=-30



% Range from source to receiver
[rng, az] = vdist2(repmat(lat, [height(source),1]),...
    repmat(lon, [height(source),1]),source.Latitude,source.Longitude);


source.Range= sqrt(rng.^2+(source.z-depth).^2);

source.trueBearing = az;
source.Range=rng;
expectedArrival = source.UTC+seconds(source.Range./c); %Check this with SAM/
source.ArrivalNL= zeros(height(source),1);
source.ArrivalRL= zeros(height(source),1);
source.Detected = zeros(height(source),1);
source.BearingEst= nan(height(source),1);
source.SNR= nan(height(source),1);
source.ReceivedTime = NaT(height(source),1);
source.expectedArrival=expectedArrival;


% % Get the bearing and ofset,
% trueBearing = asin(source.y./source.Range);
%measuredBearing = received.Angle_0

received.CallFound = zeros(height(received),1);

%% Link the source and receiver dataframs


for ii =1:1:height(received)
    
    % closest approach
    [mindiff, idx] = min(abs(expectedArrival-received.UTC(ii)));
    
    
    if seconds(mindiff)<4

        source.ArrivalNL(idx) = received.RMSnoise(ii);
        source.ArrivalRL(idx) = received.RMSsignal(ii);
        source.Detected(idx) = 1;
        %source.BearingEst(idx) = received.Angle_0(ii);
        source.SNR(idx) = received.SNR(ii);
        source.ReceivedTime(idx) = received.UTC(ii);
        received.CallFound(ii)=1;
        
        % Link to the bearing table
        [~, idx1 ] = min(abs(edgeDetection.UTC-received.UTC(ii)))
        source.BearingEst(idx) = edgeDetection.Angle_0(idx1);
        
        ii
    else
        disp('nope')
    end
    
    
    
end
figure;
scatter(source.Range, source.ArrivalRL, [], source.Detected, 'filled')
% Number of detecentions in each range bin
% Aggregate by range
sourceOrig= source;
rangeBins = [0:500:max(source.Range)+1000];
figure( 110)
subplot(2,1,1)
scatter(sourceOrig.Longitude,...
    sourceOrig.Latitude, [],sourceOrig.Detected.*sourceOrig.ArrivalRL, 'filled')
hold on; scatter(lon,lat, 'r^', 'filled')


% Update values basedon the highest received levels
[~, idx] = max(source.ArrivalRL)


% 
% trouble=-2550:-3000
% source.y(source.y <= -2550)=source.y(source.y <= -2550)+3000;
temp = sourceOrig;
badTimesTemp = badTimes;







%% Remove the periods 

badTimesTemp.End_Gap = badTimesTemp.End_Gap+0/60/60/24;

badTimesTemp.Start_Gap = badTimesTemp.Start_Gap-0/60/60/24;

% Clean out the times when the unit was down
for ii=1:height(badTimes)

    startEndtimes = [datetime(badTimesTemp.Start_Gap(ii),'ConvertFrom','datenum'),...
        datetime(badTimesTemp.End_Gap(ii),'ConvertFrom','datenum')];
    
    badIdx = find(temp.expectedArrival>=startEndtimes(1) &...
        temp.expectedArrival<=startEndtimes(2));
    if ~isempty(badIdx)
        temp(badIdx,:)=[];
    end

end



figure(110)
subplot(2,1,2)
scatter(temp.Longitude,temp.Latitude, [],temp.Detected, 'filled')
hold on; scatter(lon,lat, 'r^', 'filled')


%%
figure; scatter(temp.Longitude(temp.Detected==1),...
temp.Latitude(temp.Detected==1), [],temp.Range(temp.Detected==1).^3, 'filled')
h = colorbar;
hold on; scatter(lon,lat, 'r^', 'filled')
ylabel(h, 'Received Level (dB)')
xlabel('Longitude')
ylabel('Latitute')
source=temp;
%% Inspect prelim det fx
nPlayed=[];
nDet = [];

for ii=1:length(rangeBins)-1
    
    idx = find(source.Range>rangeBins(ii) & source.Range<=rangeBins(ii+1));
    nPlayed(ii) = length(idx);
    nDet(ii) = sum(source.Detected(idx));
    
    
end

subplot(3,1,2)
scatter(source.Longitude,source.Latitude, [],source.Detected)


figure
plot(rangeBins(1:end-1), nDet./nPlayed)
%% g(SNR)

% Since the source and receiver both played the sound we need to break it
% apart

% Figure out transmission loss
TL_coef = 20;
TL = (TL_coef*log10(source.Range(source.Detected==1)))
RL =  source.ArrivalRL(source.Detected==1);

% The noise was with the source
[aa]=find(source.Range<2000 & source.Detected==1);
SourceNL =source.ArrivalNL(aa);
clear aa
% 
% % Update noise was no longer the source

SL = RL+TL-source.ArrivalNL(source.Detected==1);
NL = source.ArrivalNL(source.Detected==1);

% Guess the source level and noise level for the missed calls
sampleIndex = randi(sum(source.Detected),[sum(~source.Detected),1]);
missedR = source.Range(source.Detected==0)
missedTL = TL_coef*log10(missedR)



missedNL = std(NL)/2*randn(1,length(missedR))'+ median(NL, 'omitnan')';
missedSL = std(SL)*randn(1,length(missedR))'+ median(SL, 'omitnan')';
missedRL =missedSL-missedTL+missedNL;


source.ArrivalNL(source.Detected==0) = missedNL;
source.ArrivalRL(source.Detected==0) = missedRL;


source.SNR1 = source.ArrivalRL-source.ArrivalNL;
figure
scatter(source.Range, source.SNR, [], source.Detected, 'filled')
xlabel('Range'); ylabel('SNR')


%source.SNR = source.ArrivalRL-source.ArrivalNL;
%source.SNR(source.SNR>100) =100;
% 
% scatter(source.Range, source.SNR, [], source.Detected, 'filled')
% scatter(source.Longitude, source.Latitude+rand([height(source),1]), [],...
%     source.Detected, 'filled')
% xlabel('longitude')

% 
% % Signal Excess
% SLNL = SL-source.ArrivalNL;
% scatter(source.Range, SLNL, [], source.Detected)
% source.SLNL = source.ArrivalRL-source.ArrivalNL;
% %source.SNR = SL-source.ArrivalNL

%% Detection Function g(SNR)

SNR_bins = min(source.SNR):5: max(source.SNR);
nPlayedr=[]
nDetr=[]
for ii = 1:length(SNR_bins)-1
    
    detectedSub = source(source.SNR>= SNR_bins(ii)...
        & source.SNR<SNR_bins(ii+1),:);
    nPlayedr(ii) = height(detectedSub)
    nDetr(ii) = sum(detectedSub.Detected)
    clear detectedSub
    
end
figure (1)
scatter(SNR_bins(1:end-1), nDetr, 'filled')
xlabel('SNR'); ylabel('Proportion Detected')

b=1; m=.4;
pdetSNR = @(x) 1./(1+exp(-(b+m*x)))

hold on
plot([-50:1:20], pdetSNR([-50:1:20]) )

% Number of detections each SNR

%% For each range get the proportion of detections and the 

nPlayed=[];
nDet = [];
minSNR = [];

for ii=1:length(rangeBins)-1
    
    idx = find(source.Range>rangeBins(ii) & source.Range<=rangeBins(ii+1));
    
    
    nPlayed(ii) = length(idx);
    nDet(ii) = sum(source.Detected(idx));
    
    % Median SNR 
    minSNR(ii) = max([0 min(source.SNR(idx))]);
    
end


figure
plot(rangeBins(1:end-1), nDet./nPlayed)

figure
plot(rangeBins(1:end-1), minSNR)

%% Detection Function g(r|SLNR)

minDetRange = min(source.Range)
maxDetRange = max(source.Range)
sourceSub = source(source.Range>minDetRange,:);

SLNRbins = min(source.SLNL):10: max(source.SLNL)+20;
RangeBins =[0:5]*1000


nPlayedr=[];
nDetr = [];
figure(1)
subplot(2,1,1)
scatter(sourceSub.Range, sourceSub.SNR, [], sourceSub.Detected, 'filled')

for ii = 1:length(SLNRbins)-1
    
dataSubSNR = source(find(source.SLNL>SLNRbins(ii) &...
        source.SLNL<=SLNRbins(ii+1)),:);

for jj=1:length(RangeBins)-1
    dataSub = dataSubSNR(find(dataSubSNR.Range>RangeBins(jj) &...
        dataSubSNR.Range<=RangeBins(jj+1)),:);


    nPlayedr(ii,jj) = height(dataSub)
    nDetr(ii,jj) = sum(dataSub.Detected)
    
    
end

end


x= repmat(1:size(nDetr,2),[(size(nDetr,1)),1]);
y= repmat(1:size(nDetr,1),[(size(nDetr,2)),1])'
  pcolor(RangeBins(1:end-1),SLNRbins(1:end-1), nDetr./nPlayedr)
% figure
% scatter([1:16]*1000,nDetr./nPlayedr,'filled')
% 
% xlabel('Range km')
% ylabel('SLNL')
% 
% theta =14000; b=30;
% pdetR = @(x) 1-exp(-(x/14000).^(-30));
% 
% hold on
% plot([0:100:17000], pdetR([0:100:17000]) )



 %% Bearing and bearing error err(SNR)
 
 

% True bearing
[~, BearingTheta] =vdist2(repmat(lat, [height(source),1]),...
    repmat(lon, [height(source),1]),source.Latitude,source.Longitude);

% Observed bearing
obsTheta =  (180*source.BearingEst/pi);
% shift it by 180 deg
source.shiftedTheta = obsTheta;
% source.shiftedTheta(obsTheta<180)=obsTheta(obsTheta<180)+180;
% source.shiftedTheta(obsTheta>=180)=obsTheta(obsTheta>=180)-180;


[min(source.shiftedTheta) max(source.shiftedTheta)]
[min(BearingTheta) max(BearingTheta)]
% 
% shiftedTruth =BearingTheta-180
% shiftedTheta(obsTheta>=-180)=obsTheta(obsTheta>=180)-180;


figure;
subplot(2,1,1)
scatter(source.Longitude, source.Latitude, [], source.shiftedTheta-180); colorbar
title('Estimated Bearing')

subplot(2,1,2)
scatter(source.Longitude(source.Detected==1), source.Latitude(source.Detected==1),...
    [],source.trueBearing(source.Detected==1)); colorbar
title('True Bearing')


%%
% Bearing error
bearingError =abs(source.trueBearing-source.shiftedTheta);

bearingError(bearingError>180)=360-bearingError(bearingError>180);
source.BearingErrordeg =bearingError;
figure
plot(shiftedTheta(source.Detected==1))
hold on;
plot(BearingTheta(source.Detected==1))

% detection subset
sourceDetected = source(source.Detected==1,:);
figure; subplot(2,1,1)
scatter(sourceDetected.SNR, sourceDetected.BearingErrordeg, [], 'filled')
xlabel('Range (m)')
ylabel('Bearing Error (degrees)')
h = colorbar;
ylabel(h, 'Received SNR (dB rms)')


% Bearing Error vs SNR
SNR_bins = -30:10:40;
bearingSNR=[];
detSNR=[];
varError =[];
for ii = 1:length(SNR_bins)-1
    detectedSub = sourceDetected(sourceDetected.SNR>= SNR_bins(ii)...
        & sourceDetected.SNR<SNR_bins(ii+1),:);
    bearingSNR(ii)=mean(detectedSub.BearingErrordeg);
    varError(ii)= std(detectedSub.BearingErrordeg);
    clear detectedSub
    
end

subplot(2,1,2)
plot(SNR_bins(1:end-1), bearingSNR);
% hold on;
% plot(SNR_bins(1:end-1), varError)
xlabel('SNR dB rms')
ylabel('Average Bearing Error (degrees)')

set ( gca, 'xdir', 'reverse' )
% 
% %% Paramaterize the damn thing
% 
% % function to estimate the bearing error as a function of SNR
% % Params slope =-1; actSNR=40, meanBerringErr = 2.5
% bearingErrFx=  @(slope,SNR,actSNR,meanBearErr)...
%     (SNR<actSNR).*(slope.*SNR+45)+...
%     (SNR>=actSNR).*meanBearErr;
% 
% % Standard devaition in bearing error increases with decreasing SNR to to
% % estimate the error variance in the above function use the following
% % parameters;
% % slope = -.7, sctSNR = 40, meanBearErr=1.25
% bearingUncertainty=  @(slope,SNR,actSNR,meanBearErr)...
%     (SNR<actSNR).*(slope*SNR+68)+...
%     (SNR>=actSNR).*meanBearErr;
% 
% 
% % Create the Detection function
% theta =14000; b=30;
% pdetR = @(x) 1-exp(-(x/14000).^(-30));
% 
% hold on
% plot([0:100:17000], pdetR([0:100:17000]) )
% 
% figure (7)
% subplot(2,1,1)
% scatter([1:16]*1000,nDetr./nPlayedr,'filled')
% hold on
% plot([0:100:17000], pdetR([0:100:17000]) )
% xlabel('range')
% ylabel('Pdet')
% 
% subplot(2,1,2)
% scatter(SNR_bins(1:end-1), bearingSNR);
% hold on
% plot(SNR_bins, bearingErrFx(-1,SNR_bins,40,2.5))
% xlabel('SNR')
% ylabel('Bearing Error')
% 
% 
% 
% %% Create a basic simulation
% 
% gridX= repmat([-30000:100:30000], [301,1]);  
% gridY= gridX';
% 
% 
% gridX= repmat([-3:1:3], [7,1]);
% gridY= gridX'
% gridR = sqrt((0-gridX).^2 + (0-gridY).^2) % range from the center in meters
% 
% scatter(gridX(:), gridY(:),[], 'k')
% 
% % Set the pile driving 15 km Away
% PileGrid
% 
% 
% 
% 
% 
% 


%% Step through each column and where there are more than five zeros in a row, and it's not the first or last bit, 
% then remove those data
% 
% almostClean =source;
% uniqueRows = unique(almostClean.x);
% dataOut = [];
% for ii=13:length(uniqueRows)
% 
%     sourceIdx = find(almostClean.x==uniqueRows(ii));
%     datasub = almostClean(sourceIdx,:);
%     
% 
%     if all(datasub.Detected==0) ||all(datasub.Detected==1)
%         disp('good')
%     else
%         disp('asdf')
%         [aa bb] = sort(datasub.y);
%         
%         datasub =datasub(bb,:);
%         x =datasub.Detected;
%         indices = cleanContig0(x, 5);
%         datasub(indices,:)=[];
%         
%         dataOut =[dataOut; datasub];
%         if~isempty(indices)
%             'yay'
%             
%             if indices(end)==length(x)
%                 disp('blarg')
%             end
%             
%             rmIdx = sourceIdx(indices);
%              almostClean(rmIdx,:)=[];
%             
%         end
%         
%     end
%     
%     
%     
% 
% end
% 
% 
% figure(110)
% subplot(3,1,3)
% scatter(dataOut.x,dataOut.y, [],dataOut.Detected, 'filled')
% hold on; scatter(lon,lat, 'r^', 'filled')
% source = dataOut;



