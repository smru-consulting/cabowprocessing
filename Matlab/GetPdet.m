%% Load the meta file(s)
close all; clear all;clc



outputTables = ['C:\Data\CABOW Field Testing\20210317 HRS\20210317_HRS.csv',;
    'C:\Data\CABOW Field Testing\20210311 HRS\20210311_HRS.csv']

 
 % Aggregate by range
 rangeBins = [0:200:2500]
 nPlayed=[];
 nDet = [];
 for ii=1:length(rangeBins)-1
 
     nPlayed(ii) = sum(TJoined.range>rangeBins(ii) & TJoined.range<=rangeBins(ii+1))
     nDet(ii) = sum(TJoined.range>rangeBins(ii) & TJoined.range<=rangeBins(ii+1)...
     &TJoined.Upcall==1)

 
 end
 
 
 propDet = nDet./nPlayed
 
 figure(2)
 plot(rangeBins(2:end), propDet)
 
 
 
 
 