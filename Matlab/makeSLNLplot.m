function makeSLNLplot(SLNRbins, RangeBins, SLtable, trim)

if nargin<4
    trim=0;
end

nPlayedr=zeros([length(SLNRbins)-1, length(RangeBins)-1]);
nDetr =zeros([length(SLNRbins)-1, length(RangeBins)-1]);
Pdet=[];
SLtableTemp= SLtable;


for ii = 1:length(SLNRbins)-1
    
    dataSubSNR = SLtableTemp(find(SLtableTemp.SLNL>SLNRbins(ii) &...
        SLtableTemp.SLNL<=SLNRbins(ii+1)),:);
    
    for jj=1:length(RangeBins)-1
        dataSub = dataSubSNR(find(dataSubSNR.range>RangeBins(jj) &...
            dataSubSNR.range<=RangeBins(jj+1)),:);
        
        
        nPlayedr(ii,jj) = nPlayedr(ii,jj)+height(dataSub);
        nDetr(ii,jj) = nDetr(ii,jj)+sum(dataSub.detected);
        
        bearingError(ii, jj) = sum(abs(dataSub.BearingError),'omitnan');
%         medBearing(ii,jj) = median(dataSub.Azmuth, 'omitnan');
    end
    
end


Pdet =  nDetr./nPlayedr;
Pdet(nPlayedr<=trim)=nan;

% Pdet(nPlayedr<5) =NaN
nPlayedr(nPlayedr==0) = nan;
pcolor(RangeBins(1:end-1),SLNRbins(1:end-1),Pdet)
ylabel('Signal Excess (SL-NL)')
xlabel('Range (m)')


end