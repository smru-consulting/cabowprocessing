
function indices = cleanContig0(x, nContig)

% x, array
% n, contiguous zeros to find. Ignores start and end


transitions = diff([0; x == 0; 0]); %find where the array goes from non-zero to zero and vice versa
runstarts = find(transitions == 1);
runends = find(transitions == -1); %one past the end

% if first set of zeros is ok, then move on
if any(runstarts ==1)
    runstarts=runstarts(2:end)
    runends=runends(2:end)
end

% if last set of zeros it's ok   
if any(runends ==(length(x)+1))
    runstarts=runstarts(1:end-1)
    runends=runends(1:end-1)
end




runlengths = runends - runstarts;
%keep only those runs of length 4 or more:
runstarts(runlengths < nContig) = [];
runends(runlengths < nContig) = [];
%expand each run into a list indices:
indices = arrayfun(@(s, e) s:e-1, runstarts, runends, 'UniformOutput', false);
indices = [indices{:}];  %concatenate the list of indices into one vector
%x(indices) = 1 %replace the indices with 2





end

