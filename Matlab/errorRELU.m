function F =  errorRELU(slope,SNR,actSNR,meanBearErr)
%  Function give an estimate of the 
%   activationVal - value (dB) at which the response goes from flat to
%   linearly increasing
% mean1- mean bearing error in the flat part of the curve
%  sigma1- standard deviation associated with the flat part of the curve
%  slope - relationship between bearing error (deg) and SNR below the
%  inflection point
%  sigma2 - bearing error associated with the inflection part of the curve


key=  @(slope,SNR,actSNR,meanBearErr)(SNR<actSNR)*(slope*SNR+45)+(SNR>=actSNR)*meanBearErr
F=key;

end
