
close all; clear all; clc

files = dir('C:\BitBucketRepositories\CABOWProcessing\ProcessedCSVfiles\*.csv')

SLtable = []

SLNRbins = 20:10:100;
RangeBins =[0:500:16000]
goodFiles=[5,6,7,8,9,12]
goodFiles=1:length(files)

for ii = 1:length(goodFiles)
    
    jj = goodFiles(ii)
    fullfile(files(jj).folder,files(jj).name)
    table = readtable(fullfile(files(jj).folder,files(jj).name));
    
    table.Unit = ones(height(table),1)*str2num(files(jj).name(end-6:end-4));
    
    SLtable= [SLtable; table];
    
    figure 
    subplot(2,1,1)
    makeSLNLplot(SLNRbins, RangeBins, table)
    title( [ files(jj).name(1:8),' ',  files(jj).name(end-6:end-4)])
    
    subplot(2,1,2)
    hold on
    scatter( table.lon, table.lat, [], table.detected, 'filled')

    hold on
    scatter(table.UnitLON(1), table.UnitLat(1), 'r^', 'filled')
    ylim([48.3 48.53])
    xlim([-123.1 -122.94])
    
end

figure; 
scatter(1:height(SLtable), SLtable.SLdB);
makeSLNLplot(SLNRbins, RangeBins, SLtable);
%% Plot Griffin Bay Field Trials
mDates = unique(floor(SLtable.matlabDate))
GiffBayDates = mDates(end-1:end)
GriffinBaySub = SLtable(SLtable.matlabDate>=GiffBayDates(1),:);


makeSLNLplot(SLNRbins, RangeBins, GriffinBaySub)
title('Griffin Bay Playbacs')
figure;
scatter(GriffinBaySub.range,    GriffinBaySub.BearingError, [],'filled')
xlabel('Range'); ylabel('Bearing Error');title('Griffin Bay Playbacs')

%%

SLtable.BearingError(SLtable.BearingError>180) = 360-SLtable.BearingError(SLtable.BearingError>180); 

SLtable.Properties.VariableNames{'lat'} = 'Lon';
SLtable.Properties.VariableNames{'lon'} = 'Lat';

SLtableOrig = SLtable;
sub317 = SLtable(SLtable.Unit==317,:);
sub325 = SLtable(SLtable.Unit==325,:);


figure; 
subplot(2,2,1)
hold on
scatter(sub325.Lat, sub325.Lon, [], sub325.detected, 'filled');
scatter(-122.98018333333, 48.40735,	'filled', 'dr')
scatter(-123.0726, 48.33225,	'filled', 'dk')
title('Unit 325')
ylabel('Latitude')


subplot(2,2,3)
hold on
scatter(sub325.Lat, sub325.Lon,[], sub325.SLNL, 'filled'); hold on
scatter(-122.98018333333, 48.40735,	'filled', 'r')
scatter(-123.0726,48.33225, 	'filled', 'k')
colorbar
ylabel('Latitude')
xlabel('Longitude')
h = colorbar;
set(get(h,'label'),'string','SLNL');

subplot(2,2,2)
hold on
scatter(sub317.Lat, sub317.Lon,[], sub317.detected, 'filled');
scatter(-122.98018333333, 48.40735,	'filled', 'k')
scatter(-123.0726, 48.33225,	'filled', 'r')
title('Unit 317')


subplot(2,2,4)
hold on
scatter(sub317.Lat, sub317.Lon,[], sub317.SLNL, 'filled'); hold on
scatter(-122.98018333333,48.40735, 	'filled', 'k')
scatter(-123.0726, 48.33225,	'filled', 'r')
colorbar
xlabel('Longitude')
h = colorbar;
set(get(h,'label'),'string','SLNL');


figure;
subplot(2,1,1)
histogram(SLtable.ReceivedTimeNoiseRMSDB)
xlabel('RMS Noise Level (dB re 1 micro Pa)')
title('Histogram of Noise Levels at the CABOW Locations')
ylabel('Number of Observations')

dataforHist =[SLtable.SLdB./(strcmp(SLtable.callType, 'Simulated')),...
    SLtable.SLdB./(strcmp(SLtable.callType, 'Parks')),...
    SLtable.SLdB./(strcmp(SLtable.callType, 'DCLDE'))]

subplot(2,1,2)
hold on
dataforHist(isinf(dataforHist))=nan;
hist(dataforHist)
xlabel('RMS Source Level (dB re 1 micro Pa)')
title('Histogram of Playback Call Source Levels ')
ylabel('Number of Observations')
legend({'Simulated', 'D-Tag', 'DCLDE'})

%% Bearing Error
figure; scatter(SLtableOrig.recSNR(SLtableOrig.detected==1),...
    SLtableOrig.BearingError(SLtableOrig.detected==1),[],...
    SLtableOrig.Unit(SLtableOrig.detected==1), 'filled')

figure;
histogram(SLtableOrig.BearingError(SLtableOrig.detected==1))

SLtableOrig
%%
close all
%   SLtable = SLtable(find(strcmp(SLtable.callType, 'Simulated')),:);

% 
%    SLtable = SLtable(SLtable.lon< -122.98,:);
%    SLtable = SLtable(SLtable.lon> -122.98,:);
% 
% minDetRange = min(SLtable.range)
% maxDetRange = max(SLtable.range)
% sourceSub = SLtable(SLtable.range>minDetRange,:);



callTypes = {'Simulated','Parks',  'DCLDE'}


nPlayedr=zeros([length(SLNRbins)-1, length(RangeBins)-1]);
nDetr =zeros([length(SLNRbins)-1, length(RangeBins)-1]);
bearingError=nDetr;
medBearing=[];
figure(14)


% 
% SLtableTemp = SLtable(~strcmp(SLtable.callType, 'Simulated'),:);
cabowUnits = unique(SLtable.Unit)

figure
for kk = 1:3
%     
%     SLtableTemp = SLtable(SLtable.Unit==cabowUnits(kk),:);
SLtableTemp = SLtable(find(strcmp(SLtable.callType, callTypes{kk})),:);

subplot(2,2,kk)
   makeSLNLplot(SLNRbins, RangeBins, SLtableTemp, 0)
   title(callTypes{kk})

end

%% By dates
SLtable.dateRound = floor(SLtable.matlabDate+(8/24))

dateVals = unique(SLtable.dateRound);

for ii =1:length(dateVals)

    SLtableTemp = SLtable(SLtable.dateRound==  dateVals(ii),:);

figure
   makeSLNLplot(SLNRbins, RangeBins, SLtableTemp)
   title(datestr(dateVals(ii)))


end




%%

Pdet =  nDetr./nPlayedr
% 
% Pdet(nPlayedr<5) =NaN
x= repmat(1:size(nDetr,2),[(size(nDetr,1)),1]);
y= repmat(1:size(nDetr,1),[(size(nDetr,2)),1])'


nPlayedr(nPlayedr==0) = nan;
figure;
subplot(2,1,1)
pcolor(RangeBins(1:end-1),SLNRbins(1:end-1),nPlayedr)
ylabel('SLNL')
xlabel('Range')
title('Number of Observations')

subplot(2,1,2)
pcolor(RangeBins(1:end-1),SLNRbins(1:end-1),Pdet)
ylabel('SLNL')
xlabel('Range')
title('Proportion Detected')

% 
% 
figure
pcolor(RangeBins(1:end-1),SLNRbins(1:end-1),bearingError./nPlayedr)
ylabel('SLNL')
xlabel('Range')
title('Average Bearing Error')
figure; scatter(RangeBins(1:end-1),Pdet(15,:), 'filled')
title('Expected Detection Function with 50 dB Signal Excess')

x = 0:200:15000;
figure; scatter(RangeBins(1:end-1),Pdet(13,:), 'filled')
hold on;
scatter(RangeBins(1:end-1),Pdet(15,:), 'filled')
plot(x, hazard([5500 5.5], x)*.9, 'r')
plot(x, hazard([4600 3.3], x)*.77, 'b')
title('Expected Detection Functions')
legend({'53 dB Signal Excess', '59 dB Signal Excess','Hazard Model 53',  'Hazard Model 59'})
xlabel('Range (m)')
ylabel('Detection Probability')
%% Figures for each


nPlayedr=zeros([length(SLNRbins)-1, length(RangeBins)-1]);
nDetr =zeros([length(SLNRbins)-1, length(RangeBins)-1]);
Pdet=[];
SLtableTemp= SLtable(SLtable.matlabDate> datenum('20210721',  'yyyymmdd'  ),:)
SLtableTemp= SLtableOrig;


for ii = 1:length(SLNRbins)-1
    
    dataSubSNR = SLtableTemp(find(SLtableTemp.SLNL>SLNRbins(ii) &...
        SLtableTemp.SLNL<=SLNRbins(ii+1)),:);
    
    for jj=1:length(RangeBins)-1
        dataSub = dataSubSNR(find(dataSubSNR.range>RangeBins(jj) &...
            dataSubSNR.range<=RangeBins(jj+1)),:);
        
        
        nPlayedr(ii,jj) = nPlayedr(ii,jj)+height(dataSub);
        nDetr(ii,jj) = nDetr(ii,jj)+sum(dataSub.detected);
        
        bearingError(ii, jj) = sum(abs(dataSub.BearingError),'omitnan');
%         medBearing(ii,jj) = median(dataSub.Azmuth, 'omitnan');
    end
    
end
Pdet =  nDetr./nPlayedr
% 
% Pdet(nPlayedr<5) =NaN
nPlayedr(nPlayedr==0) = nan;
figure;
pcolor(RangeBins(1:end-1),SLNRbins(1:end-1),Pdet)
ylabel('SLNL')
xlabel('Range')
title(['Proportion Detected: ', callTypes{kk}] )
title(['p(r|SLNL) '] )
%% Create PDET vs SNR fig

SLtable=SLtableOrig;
SLtable.SNR = SLtable.RecRMSsignal




%%
SLtable.UTC = datetime(SLtable.matlabDate, 'ConvertFrom', 'datenum')
SLtable.UTC.TimeZone='UTC'
SLtable.Local = SLtable.UTC;

SLtable.Local.TimeZone;
SLtable.Local.TimeZone
SLtable.Local.TimeZone ='America/Los_Angeles'
SLtable.matlabDateLocal = datenum(SLtable.Local);

writetable(SLtable, 'C:\BitBucketRepositories\CABOWProcessing\ProcessedCSVfiles\Combined.csv')











