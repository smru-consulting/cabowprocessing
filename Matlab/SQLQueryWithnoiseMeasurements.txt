SELECT Right_Whale.*,Grouped_RightWhales.Upcall,NARW_UDP_Classifier.Score as ClassifierScore,Clip_Noise_Measurement.signal as Signal, Clip_Noise_Measurement.noise as noise,Clip_Noise_Measurement.SNR as SNR
FROM Right_Whale
INNER JOIN Grouped_RightWhales_Children ON Right_Whale.UID=Grouped_RightWhales_Children.UID
INNER JOIN Grouped_RightWhales ON Grouped_RightWhales_Children.parentUID=Grouped_RightWhales.UID
INNER JOIN NARW_UDP_Classifier ON Right_Whale.UID=NARW_UDP_Classifier.Clip_UID
INNER JOIN Clip_Noise_Measurement ON Right_Whale.UID=Clip_Noise_Measurement.Clip_UID;
