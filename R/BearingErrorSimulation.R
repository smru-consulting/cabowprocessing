rm(list=ls())
library(ggplot2)
library(MASS)
library(scales)
library(mgcv)
library(mgcViz)
library(caret)
# Load the functions
source("C:\\BitBucketRepositories\\CABOWProcessing\\R\\simFunctions.R")

# Load the actual data
SLtable = read.csv(
  'C:\\BitBucketRepositories\\CABOWProcessing\\ProcessedCSVfiles\\MDTrials.csv')

# Organize north to south
SLtable$CABOWid=as.factor(SLtable$CABOWid)
SLtable$CABOWid <- factor(SLtable$CABOWid,
                          levels = c("324", "325", "321", "327","319"))

# Model the probability of detection vs signal excess
pDetModel <- gam(data = SLtable,
                 detected~te(range, SLNL,k = 5),
                 family = binomial)

pDetModel.glm <- glm(data = SLtable,
                 detected~range+SLNL+Truebearing,
                 family = binomial())
# save the model
saveRDS(pDetModel, "C:\\BitBucketRepositories\\CABOWProcessing\\R\\SLNLgam.rds")


# gam prediction plot
b <- getViz(pDetModel)

gamPlot<-plot(sm(b, 1), trans = inv.logit) +
  l_fitRaster() + l_fitContour() + 
  ggtitle('')+
  xlab('Range (m)')+ 
  ylab(expression("SLNR (dB"["rms"]*" )"))+
  scale_fill_viridis(name = expression("P"["det"]*""),
                     begin = .1)



# Create the figure file
jpeg(paste(figFilesloc, "GamModel.jpg", sep=""),
     width = 12, 
     height = 10, 
     units = "cm", res = 200)
print(gamPlot)
dev.off()



####################################################
## Create the plot for signal excess##
###################################################
DistBreak = seq(0, 10000, by=500) 
SLNLbreaks = seq(20, 75,by = 5)

## Bin the results
SLtable$SLNLbin = cut(as.numeric(SLtable$SLNL), 
                           breaks=SLNLbreaks, 
                           include.lowest=TRUE, 
                           right=FALSE)
SLtable$DistBin = cut(SLtable$range, 
                           breaks=DistBreak, 
                           include.lowest=TRUE, 
                           right=FALSE)

SLNRden = aggregate(data = SLtable, 
                    detected~SLNLbin+DistBin, FUN =mean)
SLNRden$Nobs = aggregate(data = SLtable, 
                         detected~SLNLbin+DistBin, FUN =length)[3]
SLNRden$Dist =DistBreak[as.numeric(SLNRden$DistBin)]
SLNRden$SLNLBinNumeric =SLNLbreaks[as.numeric(SLNRden$SLNLbin)]

SLNRden =SLNRden[SLNRden$Nobs>5,]
jet.colors <- colorRampPalette(c("#00007F", "blue", "#007FFF", "cyan", "#7FFF7F", "yellow", "#FF7F00", "red", "#7F0000"))
# Transmission loss
df = data.frame(x = seq(0,max(DistBreak)), 
                y = 16*log10(seq(0,max(DistBreak))))

SEplot<- 
  ggplot() +
  geom_tile(data = SLNRden, mapping = aes(x = Dist,
                                          y = SLNLBinNumeric,
                                          fill = detected))+
  scale_fill_gradientn(name  ="g(SLNR,r)", colours =  jet.colors(7))+
  #scale_fill_viridis_c(option="plasma")+
  theme_bw()+
  xlab('Range (m)') + 
  #theme(text=element_text(family="serif"))+
  ylab(expression("SLNR (dB"["rms"]*" )"))
  #geom_line(data = df, aes(x = x,  y = y), size=2)+
    #ylim(c(20,75))

# Create the figure file
jpeg(paste(figFilesloc, "sigExcess.jpg", sep=""),
     width = 12, 
     height = 10, 
     units = "cm", res = 200)
print(SEplot)
dev.off()



  ####################################################
  ##  Display Noise levels ##
  ###################################################  
  
  # this requires more investigation. Likely relationship between 
  # Noise level at the boat and range
  
  
  ggplot(SLtable,aes(x=as.factor(CABOWid), y=RecRMSnoise))+
    geom_violin(draw_quantiles = c(0.25, 0.5, 0.75))+
    theme_bw()+
    labs(y = 
           expression("Ambient Sound Levl (dB"[rms]^{}*"50-225Hz)"),
         x= 'CABOW ID')
  

  
  ggplot(SLtable)+
    geom_point(aes(x=log10(range), y=RecRMSnoise,
                   col = as.factor(CABOWid)))+
    geom_smooth(method = "lm", alpha = .15, 
                aes(x=log10(range), y=RecRMSnoise,
                    color = as.factor(CABOWid)))+
    scale_color_brewer(palette ='Accent')
  
  #### Get the detection Performance#################
  
 
  ####################################################
  ##  Display Bearing Error ##
  ###################################################  
   
  ggplot(SLtable,aes(x=CABOWid, y=BearingError))+
    geom_violin(draw_quantiles = c(0.25, 0.5, 0.75))+
    theme_bw()+
    ylim(c(-10,10))

  ggplot(SLtable,aes(x=log10(range), y=BearingError))+
    facet_grid(~CABOWid)+
    geom_point()+
    theme_bw()+
    ylim(c(-6,14))
  
  
  xlab <- "Bearing Error(�)"
  fullHist<-ggplot(SLtable,aes(BearingError))+
  geom_histogram(aes(y = ..density..),
                 breaks=c(seq(-180,-20,40),
                          seq(-19,19,1), 
                          seq(20,180,40)) )+
    geom_line(aes(y = ..density..), 
              colour=26, stat = 'density',
              size = .1, alpha = .6)+
    xlab('Bearing Error (Degrees)')+
    ylab('Histogram Density')+
    theme_bw()
  
  # Create the figure file
  jpeg(paste(figFilesloc, "FullHistogram.jpg", sep=""),
       width = 20, 
       height = 10, 
       units = "cm", res = 200)
  print(fullHist)
  dev.off()
  
  # Create the figure file
  jpeg(paste(figFilesloc, "croppedHistogram.jpg", sep=""),
       width = 20, 
       height = 10, 
       units = "cm", res = 200)
  print(fullHist+xlim(c(-20,20)))
  dev.off()

#### Run a simulation moving the sensor#################


p2=list()

kmFROMExclusion = seq(0,4,.3)
Pilexlocs = 10000+1000*(kmFROMExclusion)
pileY = 0
buoyX =0; buoyY =0


outputMetrics=data.frame()
for (ii in 1:length(kmFROMExclusion)){
  
  pileX=Pilexlocs[ii]
  exclusion = circleFun(center = c(pileX, pileY),
                        diameter = 20000,
                        npoints = 100)

  ## Set up the simulation
  df =createGrid( gridxMin =-20000, gridxMax = 80000,
                    gridyMin =-80000, gridyMax = 80000,
                    pileX = pileX, pileY = 0, 
                  buoyX = buoyX,
                  buoyY = buoyY, gridRes =200)
  
  ## Add an SNR column
  df <- createSNRgrid(df,SL = 145,NL=98,tlCoef=16)
  df$SLNL <-  median(SLtable$SLdB)-median(SLtable$RecRMSnoise)
  
  ## Update detections and bearing error
  df <- detectionsBearingErr(df, pDetModel)

  out <-exclusionCalcs(df,
                       pileY = pileY, pileX = pileX,
                       buoyX = buoyX, buoyY = buoyY)
  
  df=out[[1]]
  calcs=out[[2]]
  
  df$plotting <- factor(df$plotting, 
                          levels = c( "Missed" , "False Alarm", "Correct","unk"))
  df$BearingPlot <- factor(df$BearingPlot, 
                           levels = c( "Missed" , "False Alarm", "Correct","unk"))
  
  
  cbp1 <- c("#D55E00",  "#F4EDCA", "#52854C")
  custom.col <- c("#D16103","#FFDB6D", "#52854C", "#C4961A", "#F4EDCA", 
                   "#C3D7A4", "#4E84C4", "#293352")
  
  p2[[ii]]<- ggplot(data=df[df$BearingPlot != 'unk',])+
    geom_point(aes(buoyX,buoyY, color =BearingPlot))+
    #scale_color_manual(values  = custom.col )+
    scale_color_viridis_d()+
    geom_path(data = exclusion,aes(x,y))+
    guides(color=guide_legend(title="Action"))+
    theme_bw()+ xlab('meters')+ylab('meters')
    
  ggplot(data=df[df$plotting != 'unk',])+
    geom_point(aes(buoyX,buoyY, color =plotting))+
    #scale_color_manual(values  = custom.col )+
    scale_color_viridis_d()+
    geom_path(data = exclusion,aes(x,y))+
    guides(color=guide_legend(title="Action"))+
    theme_bw() +xlab('meters')+ylab('meters')

  outputMetrics=rbind(outputMetrics, out[[2]])
  
}


outputMetrics$SepDist= kmFROMExclusion
# Combine data for the f1 plot
temp = outputMetrics[, c('ModF1','SepDist', 'modPrec', 'modRecall','propCovered')]
temp$Bearing = 'Bearing'

temp1 = outputMetrics[, c('ModF1noBearing','SepDist', 'modPrecNoBearing',
                          'modRecallNoBearing','propCovered')]
temp1$Bearing = 'No Bearing'
colnames(temp1)<-colnames(temp)


temp = rbind(temp, temp1 )
temp$Bearing<-as.factor(temp$Bearing)

ggplot(temp)+
  geom_point(aes(SepDist, ModF1, color =Bearing))+
  geom_line(aes(SepDist, ModF1, color =Bearing))+
  scale_color_viridis_d()+ theme_bw()+
  ylab('Modified F1 Score')+
  xlab('Distance to Exclusion Zone Radius (km)') 
                                                                labels = trans_format("log10", math_format(10^.x))) 


ggplot(temp)+
  geom_point(aes(SepDist, ModF1, color =Bearing))+
  geom_line(aes(SepDist, ModF1, color =Bearing))+
  scale_color_viridis_d()+ theme_bw()+
  ylab('Modified F1 Score')+
  xlab('Distance to Exclusion Zone Radius (km)')+
  


ggplot(temp)+
  geom_point(aes(SepDist, falseAlarmRate, color =Bearing))+
  xlab('Distance to Exclusion Zone Radius (km)')




# Estimate SNR as a function of range

# p <- ggplot(df, aes(x = buoyX, y = buoyY))+ 
#   geom_tile(aes(fill = SNR))+
#   scale_fill_distiller(palette = "YlGnBu")+
#   xlab('meters')+ylab('meters')

# 
# # Add a circle for the pile driving apparatus
# 
# p + geom_path(data = exclusion,aes(x,y))
# 
# 
# 
# 
# 
# 
# p1 <- ggplot(df, aes(x = buoyX, y = buoyY))+ 
#   geom_tile(aes(fill = bearingError))+
#   scale_fill_distiller(palette = "YlGnBu")+
#   xlab('meters')+ylab('meters')
# 
# 
# # Add a circle for the pile driving apparatus
# p1 + geom_path(data = exclusion,aes(x,y))
# 
# 
# 
# 



  

